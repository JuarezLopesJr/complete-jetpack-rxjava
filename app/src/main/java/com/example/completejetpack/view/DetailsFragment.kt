package com.example.completejetpack.view

import android.app.PendingIntent
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.telephony.SmsManager
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.palette.graphics.Palette
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.example.completejetpack.R
import com.example.completejetpack.databinding.FragmentDetailsBinding
import com.example.completejetpack.databinding.SmsDialogBinding
import com.example.completejetpack.model.DogBreed
import com.example.completejetpack.model.SmsInfo
import com.example.completejetpack.utils.loadUrl
import com.example.completejetpack.viewmodel.DogsDetailViewModel

class DetailsFragment : Fragment(R.layout.fragment_details) {
    private var fragmentDetailsBinding: FragmentDetailsBinding? = null
    private var smsDialogBinding: SmsDialogBinding? = null
    private var dogId = 0
    private var sendSmsStarted = false
    private var currentDog: DogBreed? = null
    private val viewModel: DogsDetailViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        val binding = FragmentDetailsBinding.bind(view)
        val smsBinding = SmsDialogBinding.inflate(layoutInflater)

        fragmentDetailsBinding = binding
        smsDialogBinding = smsBinding

        arguments?.let {
            dogId = DetailsFragmentArgs.fromBundle(it).dogUuid
        }

        viewModel.fetch(dogId)
        observeViewModel()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.detail_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_send -> {
                sendSmsStarted = true
                (activity as MainActivity).checkSmsPermission()
                onPermissionResult(sendSmsStarted)
            }
            R.id.action_share -> {
                val intent = Intent(Intent.ACTION_SEND).apply {
                    type = "text/plain"
                    putExtra(Intent.EXTRA_SUBJECT, "sharing dog images")
                    putExtra(
                        Intent.EXTRA_TEXT,
                        "${currentDog?.dogBreed} bred for ${currentDog?.bredFor}"
                    )
                    putExtra(Intent.EXTRA_STREAM, currentDog?.imageUrl)
                }
                startActivity(Intent.createChooser(intent, "Share with:"))
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        fragmentDetailsBinding = null
        super.onDestroy()
    }

    private fun observeViewModel() {
        viewModel.dogLiveData.observe(viewLifecycleOwner, {
            currentDog = it
            it?.let {
                fragmentDetailsBinding!!.apply {
                    dogImage.loadUrl(it.imageUrl)
                    dogDetailName.text = it.dogBreed
                    dogPurpose.text = it.bredFor
                    dogTemperament.text = it.temperament
                    dogLifeSpan.text = it.lifeSpan
                }
                it.imageUrl?.let { color ->
                    setupBackgroundColor(color)
                }
            }
        })
    }

    private fun setupBackgroundColor(url: String) {
        Glide.with(this)
            .asBitmap()
            .load(url)
            .into(object : CustomTarget<Bitmap>() {
                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                    Palette.from(resource)
                        .generate {
                            fragmentDetailsBinding!!.detailLayout
                                .setBackgroundColor(it?.darkMutedSwatch?.rgb ?: 0)
                        }
                }

                override fun onLoadCleared(placeholder: Drawable?) {}
            })
    }

    private fun sendSMS(smsInfo: SmsInfo) {
        val intent = Intent(context, MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(context, 0, intent, 0)
        val sms = SmsManager.getDefault()
        sms.sendTextMessage(smsInfo.to, null, smsInfo.text, pendingIntent, null)
    }

    fun onPermissionResult(permissionGranted: Boolean) {
        if (sendSmsStarted && permissionGranted) {
            context?.let {
                val smsInfo = SmsInfo(
                    "",
                    "${currentDog?.dogBreed} bred for ${currentDog?.bredFor}",
                    "${currentDog?.imageUrl}"
                )

                AlertDialog.Builder(it)
                    .setView(smsDialogBinding?.root)
                    .setPositiveButton("Send SMS") { _, _ ->
                        if (smsDialogBinding!!.smsDestination.text.isNotEmpty()) {
                            smsInfo.to = smsDialogBinding!!.smsDestination.text.toString()
                            sendSMS(smsInfo)
                        }
                    }
                    .setNegativeButton("Cancel") { dialogInterface, _ ->
                        dialogInterface.cancel()
                        smsDialogBinding!!.root.removeView(view)
                    }
                    .show()

                smsDialogBinding?.apply {
                    smsDestination.setText(smsInfo.to)
                    smsText.setText(smsInfo.text)
                    smsImage.loadUrl(smsInfo.smsImageUrl)
                }
            }
        }
    }
}