package com.example.completejetpack.viewmodel

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.completejetpack.databinding.ItemDogBinding
import com.example.completejetpack.model.DogBreed
import com.example.completejetpack.utils.loadUrl
import com.example.completejetpack.view.ListFragmentDirections

class DogsListAdapter(private val dogsList: MutableList<DogBreed>) :
    RecyclerView.Adapter<DogsListAdapter.DogsListViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DogsListViewHolder {
        val itemBinding = ItemDogBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return DogsListViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: DogsListViewHolder, position: Int) {
        val dogId = dogsList[position].uuid
        holder.bind(dogsList[position])
        holder.itemView.setOnClickListener {
            Navigation.findNavController(it)
                .navigate(ListFragmentDirections.actionListFragmentToDetailsFragment(dogId))
        }
    }

    override fun getItemCount() = dogsList.size

    fun updateDogsList(newDogsList: List<DogBreed>) {
        dogsList.apply {
            clear()
            addAll(newDogsList)
        }
        notifyDataSetChanged()
    }

    class DogsListViewHolder(itemBinding: ItemDogBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        private val dogImage = itemBinding.imageView
        private val dogName = itemBinding.dogName
        private val dogGroup = itemBinding.dogGroup
        private val bredFor = itemBinding.bredFor
        private val temperament = itemBinding.temperament
        private val dogLifeSpan = itemBinding.lifeSpan

        fun bind(dog: DogBreed) {
            dogImage.loadUrl(dog.imageUrl)
            dogName.text = dog.dogBreed
            dogGroup.text = dog.breedGroup
            bredFor.text = dog.bredFor
            temperament.text = dog.temperament
            dogLifeSpan.text = dog.lifeSpan
        }
    }
}