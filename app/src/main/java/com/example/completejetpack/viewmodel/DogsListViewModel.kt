package com.example.completejetpack.viewmodel

import android.app.Application
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.completejetpack.model.DogBreed
import com.example.completejetpack.model.DogsApiService
import com.example.completejetpack.model.DogsDatabase
import com.example.completejetpack.utils.NotificationHelper
import com.example.completejetpack.utils.SharedPreferencesHelper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.launch

private const val ERROR = "FROM DOGLIST_VIEWMODEL"

class DogsListViewModel(application: Application) : AndroidViewModel(application) {
    private val disposable = CompositeDisposable()
    private var prefHelper = SharedPreferencesHelper(getApplication())
    private var refreshTime = 5 * 60 * 1000 * 1000 * 1000L // 5 min in nano-seconds

    val dogs = MutableLiveData<List<DogBreed>>()
    val dogsLoadError = MutableLiveData<Boolean>()
    val loading = MutableLiveData<Boolean>()

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }

    private fun fetchFromRemote() {
        loading.value = true
        disposable.add(
            DogsApiService.getDogsService()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<List<DogBreed>>() {
                    override fun onSuccess(dogList: List<DogBreed>) {
                        storeDogsLocally(dogList)
                        Toast.makeText(
                            getApplication(),
                            "Dogs retrieved from API",
                            Toast.LENGTH_SHORT
                        )
                            .show()
                        NotificationHelper(getApplication()).createNotification()
                    }

                    override fun onError(e: Throwable) {
                        loading.value = false
                        dogsLoadError.value = true
                        Log.e(ERROR, e.printStackTrace().toString())
                        e.printStackTrace()
                    }
                })
        )
    }

    fun refresh() {
        checkCacheDuration()
        val updateTime = prefHelper.getUpdateTime()
        if (updateTime != null && updateTime != 0L && System.nanoTime() - updateTime < refreshTime) {
            fetchFromDatabase()
        } else {
            fetchFromRemote()
        }
    }

    private fun checkCacheDuration() {
        val cachePreference = prefHelper.getCacheDuration()
        try {
            val cachePreferenceInt = cachePreference?.toInt() ?: 5 * 60
            refreshTime = cachePreferenceInt.times(1000 * 1000 * 1000L)
        } catch (e: NumberFormatException) {
            e.printStackTrace()
        }
    }

    fun refreshBypassCache() = fetchFromRemote()

    private fun fetchFromDatabase() {
        loading.value = true
        viewModelScope.launch {
            val dogs = DogsDatabase(getApplication()).dogDao().getAll()
            dogsRetrieved(dogs)
            Toast.makeText(getApplication(), "Dogs retrieved from DB", Toast.LENGTH_SHORT)
                .show()
        }
    }

    private fun dogsRetrieved(list: List<DogBreed>) {
        loading.value = false
        dogsLoadError.value = false
        dogs.value = list
    }

    private fun storeDogsLocally(list: List<DogBreed>) {
        viewModelScope.launch {
            val dao = DogsDatabase(getApplication()).dogDao()
            dao.deleteAllDogs()
            val result = dao.insertAll(*list.toTypedArray())
            var i = 0
            while (i < list.size) {
                list[i].uuid = result[i].toInt()
                ++i
            }
            dogsRetrieved(list)
        }
        prefHelper.savedUpdateTime(System.nanoTime())
    }
}

/*val dog1 = DogBreed(
           "1",
           "Corgi",
           "12",
           "breed group",
           "bred for",
           "temperament",
           "null"
       )

       val dog2 = DogBreed(
           "2",
           "Pugi",
           "12",
           "breed group",
           "bred for",
           "temperament",
           "null"
       )

       val dog3 = DogBreed(
           "3",
           "Fila",
           "12",
           "breed group",
           "bred for",
           "temperament",
           "null"
       )

       val dog4 = DogBreed(
           "4",
           "Beagle",
           "12",
           "breed group",
           "bred for",
           "temperament",
           "null"
       )

       val dogList = arrayListOf(dog1, dog2, dog3, dog4)

        dogs.value = dogList
        dogsLoadError.value = false
        loading.value = false
*/