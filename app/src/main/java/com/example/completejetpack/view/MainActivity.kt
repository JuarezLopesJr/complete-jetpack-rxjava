package com.example.completejetpack.view

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupActionBarWithNavController
import com.example.completejetpack.R
import com.example.completejetpack.databinding.ActivityMainBinding
import com.example.completejetpack.utils.PERMISSION_SEND_SMS

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root

        binding.apply {
            setContentView(view)
            navController = (supportFragmentManager
                .findFragmentById(R.id.fragmentContainerView) as NavHostFragment)
                .navController

            setupActionBarWithNavController(navController, null)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp() || super.onSupportNavigateUp()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            PERMISSION_SEND_SMS -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    notifyDetailFragment(true)
                } else {
                    notifyDetailFragment(false)
                }
            }
        }
    }

    fun checkSmsPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS)
            != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    Manifest.permission.SEND_SMS
                )
            ) {
                AlertDialog.Builder(this)
                    .setTitle("Send SMS permission")
                    .setMessage("This app needs permission to send SMS")
                    .setPositiveButton("Ask me") { _, _ ->
                        requestSMSPermission()
                    }
                    .setNegativeButton("No") { _, _ ->
                        notifyDetailFragment(false)
                    }
                    .show()
            } else {
                requestSMSPermission()
            }
        } else {
            notifyDetailFragment(true)
        }
    }

    private fun notifyDetailFragment(permissionGranted: Boolean) {
        val activeFragment = supportFragmentManager.primaryNavigationFragment
        if (activeFragment is DetailsFragment) {
            activeFragment.onPermissionResult(permissionGranted)
        }
    }

    private fun requestSMSPermission() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(Manifest.permission.SEND_SMS), PERMISSION_SEND_SMS
        )
    }
}