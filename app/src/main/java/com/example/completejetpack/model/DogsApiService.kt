package com.example.completejetpack.model

import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object DogsApiService {
    private const val BASE_URL = "https://raw.githubusercontent.com"

    private val dogApiConfig = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
        .create(DogsApi::class.java)

    fun getDogsService(): Single<List<DogBreed>> {
        return dogApiConfig.getDogsApi()
    }
}