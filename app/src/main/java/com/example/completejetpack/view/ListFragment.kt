package com.example.completejetpack.view

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.completejetpack.R
import com.example.completejetpack.databinding.FragmentListBinding
import com.example.completejetpack.viewmodel.DogsListAdapter
import com.example.completejetpack.viewmodel.DogsListViewModel

class ListFragment : Fragment(R.layout.fragment_list) {
    private var fragmentListBinding: FragmentListBinding? = null
    private lateinit var dogsListAdapter: DogsListAdapter
    private val viewModel: DogsListViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        val binding = FragmentListBinding.bind(view)
        fragmentListBinding = binding
        setupRecyclerViewAdapter()
        viewModel.refresh()

        swipeToRefreshListener()
        observeViewModel()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.settings_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.settings -> {
                view?.let {
                    Navigation.findNavController(it)
                        .navigate(ListFragmentDirections.actionListFragmentToSettingsFragment())
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        fragmentListBinding = null
        super.onDestroy()
    }

    private fun swipeToRefreshListener() {
        fragmentListBinding!!.apply {
            refreshLayout.setOnRefreshListener {
                dogsList.visibility = View.GONE
                listError.visibility = View.GONE
                loadingView.visibility = View.VISIBLE
                viewModel.refreshBypassCache()
                refreshLayout.isRefreshing = false
            }
        }
    }

    private fun setupRecyclerViewAdapter() = fragmentListBinding!!.dogsList.apply {
        dogsListAdapter = DogsListAdapter(mutableListOf())
        adapter = dogsListAdapter
        layoutManager = LinearLayoutManager(context)
    }

    private fun observeViewModel() {
        viewModel.dogs.observe(viewLifecycleOwner, {
            it?.let {
                fragmentListBinding!!.dogsList.visibility = View.VISIBLE
                dogsListAdapter.updateDogsList(it)
            }
        })

        viewModel.dogsLoadError.observe(viewLifecycleOwner, {
            it?.let {
                fragmentListBinding!!.listError.visibility = if (it) View.VISIBLE else View.GONE
                if (it) {
                    fragmentListBinding!!.loadingView.visibility = View.GONE
                    fragmentListBinding!!.dogsList.visibility = View.GONE
                }
            }
        })

        viewModel.loading.observe(viewLifecycleOwner, {
            it?.let {
                fragmentListBinding!!.loadingView.visibility = if (it) View.VISIBLE else View.GONE
                if (it) {
                    fragmentListBinding!!.listError.visibility = View.GONE
                    fragmentListBinding!!.dogsList.visibility = View.GONE
                }
            }
        })
    }
}