package com.example.completejetpack.model

import io.reactivex.Single
import retrofit2.http.GET

interface DogsApi {
    @GET("DevTides/DogsApi/master/dogs.json")
    fun getDogsApi(): Single<List<DogBreed>>
}