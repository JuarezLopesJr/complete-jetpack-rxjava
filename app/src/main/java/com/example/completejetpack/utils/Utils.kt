package com.example.completejetpack.utils

import android.content.Context
import android.widget.ImageView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.completejetpack.R

const val PERMISSION_SEND_SMS = 42

fun ImageView.loadUrl(url: String?, errorDrawable: Int = R.drawable.ic_launcher_foreground) {
    /* context comes from ImageView scope */
    context?.let {
        val options = RequestOptions()
            .placeholder(getProgressDrawable(context))
            .error(errorDrawable)

        Glide.with(context.applicationContext)
            .load(url)
            .apply(options)
            .into(this)
    }
}

fun getProgressDrawable(context: Context): CircularProgressDrawable {
    return CircularProgressDrawable(context).apply {
        strokeWidth = 5f // or 10f
        centerRadius = 30f
        setColorSchemeColors(R.color.teal_700)
        start()
    }
}
