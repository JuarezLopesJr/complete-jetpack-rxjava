package com.example.completejetpack.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.completejetpack.model.DogBreed
import com.example.completejetpack.model.DogsDatabase
import kotlinx.coroutines.launch

class DogsDetailViewModel(application: Application) : AndroidViewModel(application) {
    val dogLiveData = MutableLiveData<DogBreed>()

    fun fetch(dogId: Int) {
        viewModelScope.launch {
            val dog = DogsDatabase(getApplication()).dogDao().getDog(dogId)
            dogLiveData.value = dog
        }
    }
}

/*val dog = DogBreed(
            "1",
            "Corgi",
            "12",
            "breed group",
            "bred for",
            "temperament",
            "null"
        )*/